package buu.supawee.androidplusgame

import android.content.Context
import android.content.SharedPreferences
import android.util.Log

class PreferenceClass (context: Context){

    val preferenceName = "plus"
    private val pref_correctPlusGame = "correct"
    private val pref_incorrectPlusGame = "incorrect"

    val pref:SharedPreferences = context.getSharedPreferences(preferenceName,Context.MODE_PRIVATE)

    fun getCorrectPlus() : Int{
        return pref.getInt(pref_correctPlusGame,0)
    }

    fun clearCache(){
        pref.edit().clear().commit()
        Log.i("test","clear")
    }

    fun setCorrect(correct:Int){
        val editor = pref.edit()
        editor.putInt(pref_correctPlusGame.toString(),correct)
        editor.commit()
        Log.i("test","${pref.all}")
    }

    fun getIncorrectPlus() : Int{
        return pref.getInt(pref_incorrectPlusGame.toString(),0)
    }

    fun setIncorrect(incorrect:Int){
        val editor = pref.edit()
        editor.putInt(pref_incorrectPlusGame,incorrect)
        editor.commit()
        Log.i("test","${pref.all}")
    }

}