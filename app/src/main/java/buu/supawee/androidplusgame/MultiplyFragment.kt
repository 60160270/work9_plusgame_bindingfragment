package buu.supawee.androidplusgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import buu.supawee.androidplusgame.databinding.FragmentGameBinding
import buu.supawee.androidplusgame.databinding.FragmentMultiplyBinding

class MultiplyFragment : Fragment() {
    lateinit var binding: FragmentMultiplyBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_multiply, container, false)
        var resultNumber = gameStart()
        val pref = PreferenceClass(requireContext())
        var won = pref.getCorrectPlus()
        var lose = pref.getIncorrectPlus()

        binding.apply {
            txtCorrect.text = won.toString()
            txtIncorrect.text = lose.toString()
            ans1.setOnClickListener {
                if(ans1.text=="${resultNumber}"){
                    won += 1
                    txtCorrect.text = "${won}"
                    result.text = "Correct !"
                    pref.setCorrect(won)
                }else{
                    lose += 1
                    txtIncorrect.text = "${lose}"
                    result.text = "Incorrect !"
                    pref.setIncorrect(lose)
                }

                Toast.makeText(activity,"${pref.getCorrectPlus()} ${pref.getIncorrectPlus()}",
                    Toast.LENGTH_LONG).show()

                txtCorrect.text = won.toString()
                txtIncorrect.text = lose.toString()
                resultNumber = gameStart()
            }
            ans2.setOnClickListener {
                if(ans2.text=="${resultNumber}"){
                    won += 1
                    txtCorrect.text = "${won}"
                    result.text = "Correct !"
                    pref.setCorrect(won)
                }else{
                    lose += 1
                    txtIncorrect.text = "${lose}"
                    result.text = "Incorrect !"
                    pref.setIncorrect(lose)
                }

                txtCorrect.text = won.toString()
                txtIncorrect.text = lose.toString()
                resultNumber = gameStart()
            }
            ans3.setOnClickListener {
                if(ans3.text=="${resultNumber}"){
                    won += 1
                    txtCorrect.text = "${won}"
                    result.text = "Correct !"
                    pref.setCorrect(won)
                }else{
                    lose += 1
                    txtIncorrect.text = "${lose}"
                    result.text = "Incorrect !"
                    pref.setIncorrect(lose)
                }

                txtCorrect.text = won.toString()
                txtIncorrect.text = lose.toString()
                resultNumber = gameStart()
            }
        }

        // Inflate the layout for this fragment
        return binding.root
    }
    private fun gameStart():Int{
        val x = (0..10).random()
        val y = (0..10).random()
        binding.apply {
            number1?.text = "${x}"
            number2?.text = "${y}"
            randomChoice(x*y)
        }
        return x*y
    }
    private fun randomChoice(resultNumber : Int){
        binding.apply {
            val randomChoice = (1..3).random()
            if(randomChoice == 1){
                ans1?.text = "${resultNumber}"

                ans2?.text = "${resultNumber+1}"

                ans3?.text = "${resultNumber+2}"

            }else if(randomChoice == 2){
                ans1?.text = "${resultNumber-1}"

                ans2?.text = "${resultNumber}"

                ans3?.text = "${resultNumber+1}"
            }else{

                ans1?.text = "${resultNumber-2}"

                ans2?.text = "${resultNumber-1}"

                ans3?.text = "${resultNumber}"
            }
        }
    }
}